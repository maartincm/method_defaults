import os
import pytest

from .. import config_manager
from ..decorators import defaults


DATASETFILE = 'config_dataset.yaml'


@pytest.fixture(scope="module")
def config(fp=DATASETFILE):
    filepath = os.path.join(
        os.path.dirname(__file__),
        fp)
    cfg = config_manager.Config(filepath)
    return cfg


def _foo(*args, **kwargs):
    return any(args + tuple(kwargs.values()))


class TestMethodDefaults(object):
    _name = 'TestClass'

    def test_parameter_from_file(self, config):
        @defaults(os.path.join(os.path.dirname(__file__), DATASETFILE))
        def foo(param1, hello):
            assert param1 == 'Hi'
            assert hello == config.dot.hello == 'world'
            return _foo(param1, hello)

        res = foo('Hi')
        assert res

    def test_parameter_from_config(self, config):
        @defaults(config)
        def foo(param1, hello):
            assert param1 == 'Hi'
            assert hello == config.dot.hello == 'world'
            return _foo(param1, hello)

        res = foo('Hi')
        assert res

    def test_parameter_from_subsection(self, config):
        @defaults(config, sections="section1")
        def foo(param1, listexample):
            assert listexample == config.dot.section1.listexample == \
                ["1", "2", "3", "5"]
            return _foo(param1, listexample)

        res = foo('Hi')
        assert res

    def test_parameter_from_nested_subsection(self, config):
        @defaults(config, sections="section3.s3nested2.s3n2nestedagain")
        def foo(param1, s3n2naval):
            assert s3n2naval == \
                config.dot.section3.s3nested2.s3n2nestedagain.s3n2naval == \
                "nestednested"
            return _foo(param1, s3n2naval)

        res = foo('Hi')
        assert res

    def test_parameters_from_multiple_sections(self, config):
        @defaults(config, sections=["section1", "section4"])
        def foo(value1, s4v1):
            assert value1 == config.dot.section1.value1 == "val1"
            assert s4v1 == config.dot.section4.s4v1 == "s4v1"
            return _foo(value1, s4v1)

        res = foo()
        assert res

    def test_multiple_methods(self, config):
        @defaults(config, sections=["section1", "section4"])
        def foo1(value1):
            assert value1 == config.dot.section1.value1 == "val1"
            return _foo(value1)

        @defaults(config, sections="section2")
        def foo2(value4):
            assert value4 == config.dot.section2.value4 == "val4"
            return _foo(value4)

        res = foo1()
        assert res
        res = foo2()
        assert res

    def test_parameter_type_inference(self, config):
        @defaults(config, sections="section1")
        def foo(value1: str, value2: int, listexample: list, twoeqfour: bool):
            assert value1 == "val1"
            assert value2 == 2
            assert listexample == ["1", "2", "3", "5"]
            assert twoeqfour is False
            return _foo(value1, value2, listexample, twoeqfour)

        res = foo()
        assert res

    def test_dict_inside_lists_annotation(self, config):
        @defaults(config, sections="sectionlist")
        def foo(listofdict: [dict], randomval: int):
            assert listofdict == [{
                'key1': 1,
                'key2': [1, 2, 3],
            }, {
                '1': 'valueforkey1',
                '3': 'valueforkey3',
            }]
            assert randomval == 25
            return _foo(listofdict, randomval)

        res = foo()
        assert res

    def test_keyword_passed_and_in_config(self, config):
        """ Keyword passed should be more relevant """
        @defaults(config, sections="section1")
        def foo(value1: str, value2: int = 125):
            assert value2 == 125
            return _foo(value2)

        res = foo(value2=125)
        assert res

    def test_wrong_filepath_raise(self):
        with pytest.raises(Exception):
            assert defaults('this_path_does_not_exist',
                            sections="section1")(print)
