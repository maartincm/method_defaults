import os
# import pytest

from .. import config_manager


DATASETFILE = 'config_dataset'


class TestConfigManager(object):
    _name = 'TestClass'

    def _assert_basic_dataset(self, config):
        assert config.dot.hello == 'world'

        section1 = config.dot.section1
        assert section1.value1 == 'val1'
        assert section1.value2 == '2'  # str, not int
        assert section1.listexample == ["1", "2", "3", "5"]

        section2 = config.dot.section2
        assert section2.value3 == '3'
        assert section2.value4 == 'val4'

        section3 = config.dot.section3
        assert section3.s3v1 == 's3v1'
        s3nested1 = section3.s3nested1
        assert s3nested1.s3n1val == 'nested'
        s3nested2 = section3.s3nested2
        assert s3nested2.s3n2val == 'nested'
        assert s3nested2.s3n2nestedagain.s3n2naval == 'nestednested'

        section4 = config.dot.section4
        assert section4.s4v1 == 's4v1'

    def run_test_file_by_extension(self, ext):
        filepath = os.path.join(
            os.path.dirname(__file__),
            DATASETFILE + '.' + ext)
        config = config_manager.Config(filepath)
        self._assert_basic_dataset(config)
        return True

    def test_ini_file(self):
        self.run_test_file_by_extension('ini')

    def test_json_file(self):
        self.run_test_file_by_extension('json')

    def test_yaml_file(self):
        self.run_test_file_by_extension('yaml')
